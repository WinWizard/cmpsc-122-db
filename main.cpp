/*********************************************************
 * file name: main.cpp
 * programmer name: Dylan Kozicki & Zachary McMinn & Ryan Ousey
 * date created: February 21, 2014
 * date of last revision: February 21, 2014
 * details of the revision: none
 * short description:  <enter description here>
 **********************************************************/

#include <iostream>
#include"Objective.h"
#include"Date.h"
#include"node1.h"
using namespace std;
using Cmpsc122Project::Objective;
using Cmpsc122Project::Task;

//Making the node class functions work easier
using Cmpsc122Project::node;
using Cmpsc122Project::list_length;
using Cmpsc122Project::list_head_insert;
using Cmpsc122Project::list_insert;
using Cmpsc122Project::list_search;
using Cmpsc122Project::list_locate;
using Cmpsc122Project::list_head_remove;
using Cmpsc122Project::list_remove;
using Cmpsc122Project::list_clear;

//Function Prototypes
void programDescription();
void addObjective(node*&head);
void editObjective(node*&head,node*&current);
void editTasks(Objective& nodeData);
void deleteObjective(node*&head,node*&current);
void displayObjective(Objective nodeData);
void displayAllAbridged(node*&head);
void displayAllFull(node*&head);
void writeFile(node*&head,char filePath[]);
void readFile(node*&head,char filePath[]);

void getName(string&strInput);
void getDesc(string&strInput);
void getCat(int&numInput);
void getPrio(int&numInput);
void getMonthDayYearStart(int&month,int&day,int&year);
void getMonthDayYearDue(int&month,int&day,int&year);
void getTaskMonthDayYearStart(int&month,int&day,int&year);
void getTaskMonthDayYearDue(int&month,int&day,int&year);
void getTime(int&numInput);
void getResources(Objective&listData,string&strInput,int&numInput);
void getTaskResources(Task&temp);
void getTasks(Objective&listData,string&strInput,int&numInput);
void getStatus(int&numInput);
void chooseObjective(node*&head,node*&current);
void editWhatPart(Objective nodeData);
void editResources(Objective& nodeData);
void addResource(Objective& nodeData);
void deleteResourcePos(Objective& nodeData);
void deleteResourceNam(Objective& nodeData);
void deleteTaskPos(Objective& nodeData);
void displayTaskNames(Objective& nodeData);
void editWhatTask(Objective& nodeData);
void displayTasksInFull(Objective& nodeData);
void displayTask(Task& task);
void editTaskValues(Task& task);
void editTaskResources(Task& task);
void displayTaskResources(Task& task);
void deleteTaskResourcePos(Task& task);
void deleteTaskResourceNam(Task& task);
void getFile(char filePath[]);
void clearBuffer();

int main() {
    // Program description
    programDescription();

    // Declaring the variables: types and names
	node*head=NULL,
            *current=NULL;
	int choice;
        char filePath[40];

    // Variable initialization: getting the input from the user
	do{
		cout<<"\n\nWhat would you like to do?\n"
			<<"1  Add an objective\n"
			<<"2  Edit an objective\n"
			<<"3  Delete and objective\n"
			<<"4  Display an objective\n"
			<<"5  Display all objectives (abridged)\n"
			<<"6  Display all objectives (full)\n"
			<<"7  Write the list of objectives to a file\n"
			<<"8  Retrieve a list of objectives from a file\n"
			<<"-1 Close the program\n"
			<<">";
		cin>>choice;

		
    //calculations and output

		switch(choice){
		case -1:
			cout<<"Terminating Program\n\n";
			break;
		case 1:
			addObjective(head);
			break;
		case 2:
			editObjective(head,current);
			break;
		case 3:
                        chooseObjective(head,current);
                        deleteObjective(head,current);
                        break;
		case 4:
			chooseObjective(head,current);
			displayObjective(current->data());
			break;
		case 5:
			displayAllAbridged(head);
			break;
		case 6:
			displayAllFull(head);
			break;
		case 7:
                        getFile(filePath);
                        writeFile(head,filePath);
			break;
		case 8:
			getFile(filePath);
                        readFile(head,filePath);
			break;
                default:
                        cout<<"Invalid Choice\n\n";
		}

	}while(choice!=0);

    return 0;
}//main

void programDescription() {
    cout << "This program will allow you to keep track of different objectives\n"
            <<"along with descriptions and due dates for each\n\n";
}//programDescription

void addObjective(node*&head){
    
    node*current=head;
    Objective temp;
    
    string strInput;
    int numInput;
    int day,month,year;
    
    clearBuffer();
    
    //NAME
    getName(strInput);
    temp.setName(strInput);       //set name
    
    //DESCRIPTION
    getDesc(strInput);
    temp.setDesc(strInput);       //set description
    
    //CATEGORY
    getCat(numInput);
    temp.setCat(numInput);   //set category
    
    //Priority
     getPrio(numInput);
     temp.setPrio(numInput);          //set priority
       
     
     //START DATE
     getMonthDayYearStart(month,day,year);
     temp.setStartDate(month,day,year);                   //set startDat
     
     //DUE DATE
     getMonthDayYearDue(month,day,year);
     temp.setDueDate(month,day,year);                   //set dueDate
     
     //TIME
     getTime(numInput);
     temp.setTime(numInput);
     
     //RESOURCES
     getResources(temp,strInput,numInput);
     
     //TASKS
     getTasks(temp,strInput,numInput);
     
     //STATUS
     getStatus(numInput);
     temp.setStatus(numInput);
     
     if(head!=NULL){
         while(current->link()!=NULL){
             current=current->link();
         }
         list_insert(current,temp);
     }
     else
         list_head_insert(head,temp);
     
     cout<<"\n\nObjective added...\n\n";
    return;
}//addObjective

void editObjective(node*&head,node*&current){
    if(head==NULL)
        cout<<"\nERROR:The list is empty. There is nothing to edit\n\n";
 
    else{
        chooseObjective(head,current);
        editWhatPart(current->data());
    }
    
}//editObjective

void deleteObjective(node*&head,node*&current){
    node*remove=head;
    
    while(remove->link()!=current)
        remove=remove->link();
    
    list_remove(remove);
    
}//deleteObjective

void displayObjective(Objective nodeData){
    
    cout<<"\nNAME: "<<nodeData.getName()
        <<"\nDESCRIPTION: "<<nodeData.getDesc()
        <<"\nCATEGORY: "<<nodeData.getCatAsString()
        <<"\nPRIORITY: "<<nodeData.getPrio()
        <<"\nSTART DATE: "<<nodeData.getStartDate().toString()
        <<"\nDUE DATE: "<<nodeData.getDueDate().toString()
        <<"\nTIME REQUIRED: "<<nodeData.getTime()
        <<"\nRESOURCES: \n";
    
    string* l = nodeData.getRes();
    for(int i=0;i<nodeData.getRUsed();i++) {
        cout << l[i] << endl;
    }

    cout<<"\nTASKS: \n";
    displayTasksInFull(nodeData);
    
    cout<<"COMPLETED?: ";
    if(nodeData.getStatus())
        cout<<"YES\n\n";
    else
        cout<<"NO\n\n";
    
}//displayObjective

void displayAllAbridged(node*&head){
    
    if(head==NULL)
        cout<<"\nERROR:The list is empty. There is nothing to display\n\n";
    else{
        int i=1;
        for(node* current=head;current!=NULL;current=current->link()){
            cout<<i<<".) "<<current->data().getName()<<"\n";
            i++;
        }
    }
    
}//displayAllAbridged

void displayAllFull(node*&head){
    if(head==NULL)
        cout<<"\nERROR:The list is empty. There is nothing to display\n\n";
    else{
        for(node* current=head;current!=NULL;current=current->link())
            displayObjective(current->data());
    }
}//displayAllFull

void writeFile(node*&head, char filePath[]){
    if(head==NULL)
        cout<<"\nERROR:The list is empty. There is nothing to write\n\n";
    else{
        head->data().clearOutStream(filePath);  //opens and closes file to clear all data
        for(node* temp = head;temp != NULL;temp=temp->link())
               temp->data().writeData(filePath);
    }
}//WriteFile

void readFile(node*&head,char filePath[]){
    ifstream iFile(filePath);
    if(iFile.fail()){
        cout << "No input file found";
        return;
    }
    int num;
    iFile >> num;
    node* current = head;
    while(current->link()!=NULL){
        current=current->link();
    }
    for(int i=0;i < num;i++){
        Objective tempO;
        tempO.readData(iFile);
        if(current==NULL)
            list_head_insert(current,tempO);
        else
            list_insert(current,tempO);
        current=current->link();
    }
    iFile.close();
}//ReadFile*/

void getName(string&strInput){ 
    cout<<"What is the name of your new objective?: ";
    getline(cin,strInput);
}//getName

void getDesc(string&strInput){
    cout<<"Add a description: ";
    getline(cin,strInput);
}//getDesc

void getCat(int&numInput){
    do{
          cout<<"Set a category: \n"
                 <<"1 Urgent-Important\n"
                 <<"2 Not urgent-Important\n"
                 <<"3 Urgent-Not important\n"
                 <<"4 Not urgent-Not important\n"
                 <<">";
          cin>>numInput;
          
         if(numInput<1 || numInput>4)        //Format the loop
             cout<<"Invalid Category\n\n";
    
    }while(numInput<1 || numInput>4);   //make sure numInput is legal
}//getCat

void getPrio(int&numInput){
    do{
         cout<<"Assign the new objective a priority (1-5): ";
         cin>>numInput;
         
         if(numInput<1 || numInput>5)        //Format the loop
             cout<<"Invalid Priority\n\n";
         
     }while(numInput<1 || numInput>5);      //make sure numInput is legal
}//getPrio

void getMonthDayYearStart(int&month,int&day,int&year){
    do{
         cout<<"Set the objective's start date: \n"
                 <<"Input month: ";
         cin>>month;
         if(month>13 || month<0){
             cout<<"\n\nInvalid Month\n\n";     //check month
             
         }
    }while(month>12 || month<0);
         
    do{
         cout<<"Input day: ";
         cin>>day;
         if(day<0 || day>32){
             cout<<"\n\nInvalid Day\n\n";       //check day
         }
    }while(day<0 || day>31);
         
    do{
         cout<<"Input year: ";
         cin>>year;
         if(year<0){                            //check year
             cout<<"\n\nInvalid Year\n\n";
         }
     }while(year<0);
}//getMonthDayYearStart

void getMonthDayYearDue(int&month,int&day,int&year){
    do{
         cout<<"Set the objective's due date: \n"
                 <<"Input month: ";
         cin>>month;
         if(month>13 || month<0){
             cout<<"\n\nInvalid Month\n\n";     //check month
             
         }
    }while(month>12 || month<0);
         
    do{
         cout<<"Input day: ";
         cin>>day;
         if(day<0 || day>32){
             cout<<"\n\nInvalid Day\n\n";       //check day
         }
    }while(day<0 || day>31);
         
    do{
         cout<<"Input year: ";
         cin>>year;
         if(year<0){                            //check year
             cout<<"\n\nInvalid Year\n\n";
         }
     }while(year<0);
}//getMonthDayYearDue

void getTaskMonthDayYearStart(int&month,int&day,int&year){
    do{
         cout<<"Set the task's start date: \n"
                 <<"Input month: ";
         cin>>month;
         if(month>13 || month<0){
             cout<<"\n\nInvalid Month\n\n";     //check month
             
         }
    }while(month>12 || month<0);
         
    do{
         cout<<"Input day: ";
         cin>>day;
         if(day<0 || day>32){
             cout<<"\n\nInvalid Day\n\n";       //check day
         }
    }while(day<0 || day>31);
         
    do{
         cout<<"Input year: ";
         cin>>year;
         if(year<0){                            //check year
             cout<<"\n\nInvalid Year\n\n";
         }
     }while(year<0);
}//getTaskMonthDayYearStart

void getTaskMonthDayYearDue(int&month,int&day,int&year){
    do{
         cout<<"Set the task's due date: \n"
                 <<"Input month: ";
         cin>>month;
         if(month>13 || month<0){
             cout<<"\n\nInvalid Month\n\n";     //check month
             
         }
    }while(month>12 || month<0);
         
    do{
         cout<<"Input day: ";
         cin>>day;
         if(day<0 || day>32){
             cout<<"\n\nInvalid Day\n\n";       //check day
         }
    }while(day<0 || day>31);
         
    do{
         cout<<"Input year: ";
         cin>>year;
         if(year<0){                            //check year
             cout<<"\n\nInvalid Year\n\n";
         }
     }while(year<0);
}//getTaskMonthDayYearDue

void getTime(int&numInput){
    cout<<"Please note the number of hours required: ";
     cin>>numInput;
}//getTime

void getResources(Objective&listData,string&strInput,int&numInput){
    
    do{
         clearBuffer();
         cout<<"\nAdd a resource that the objective needs: ";
         getline(cin,strInput);
         
         
         listData.addResource(strInput);
         
         do{
                cout<<"\nWould you like to add another?(1 for yes, 0 for no): ";
                cin>>numInput;
         
                if(numInput!=0&&numInput!=1)
                     cout<<"\n\nValue must be either 1 or 0\n\n";
         }while(numInput!=0 && numInput!=1);
         
     }while(numInput!=0);
     
}//getResources

void getTasks(Objective&nodeData,string&strInput,int&numInput){
    Task temp;
    int month,day,year;
    do{
        clearBuffer();
        cout<<"What is the name of the task?: ";
        getline(cin,strInput);

        temp.setName(strInput);

        cout<<"What is the description for the task?: ";
        getline(cin,strInput);

        temp.setDesc(strInput);

        do{
            cout<<"Is the task completed? (1 for yes, 0 for no): ";
            cin>>numInput;
            clearBuffer();

            if(numInput!=1 && numInput!=0)
                cout<<"\nERROR: Value must be a 1 or a 0\n\n";

        }while(numInput!=1 && numInput!=0);

        temp.setStatus(numInput);

        cout<<"How many hours will the task take to complete?: ";
        cin>>numInput;
        clearBuffer();

        temp.setTime(numInput);

        getTaskMonthDayYearStart(month,day,year);
        temp.setStartDate(month,day,year);

        getTaskMonthDayYearDue(month,day,year);
        temp.setDueDate(month,day,year);

        getTaskResources(temp);
        
        nodeData.addTask(temp);
        
        cout<<"Would you like to add another task to the objective?(0 for no): ";
        cin>>numInput;
        clearBuffer();
        
        if(numInput==0)
            return;
        
    }while(true);
}//getTasks

void getTaskResources(Task&temp){
    string strInput;
    int numInput;
    do{
        clearBuffer();
        cout<<"\nAdd a resource that the task needs: ";
        getline(cin,strInput);
         
         
        temp.addResource(strInput);
         
        do{
            cout<<"\nWould you like to add another?(1 for yes, 0 for no): ";
            cin>>numInput;
         
            if(numInput!=0&&numInput!=1)
                cout<<"\n\nValue must be either 1 or 0\n\n";
            
        }while(numInput!=0 && numInput!=1);
         
    }while(numInput!=0);
}//getTaskResources

void getStatus(int&numInput){
    do{
         
         cout<<"\nCompletion status (1 for true, 0 for false): ";
         cin>>numInput;
         
         if(numInput!=0 && numInput!=1){                            //check for bool value
             cout<<"\n\nValue must be either 1 or 0\n\n";
         }
     }while(numInput!=0 && numInput!=1);
}//getStatus

void chooseObjective(node*&head,node*&current){
    int numInput;
    
    do{
        cout<<"Choose Objective:\n";
        displayAllAbridged(head);
        cout<<">";
        cin>>numInput;
        
        if(numInput<0 || numInput>list_length(head))
            cout<<"\nERROR:Invalid choice\n\n";
                    
    }while(numInput<0 || numInput>list_length(head));
    
    current=list_locate(head,numInput);
            
}//chooseObjective

void editWhatPart(Objective nodeData){
    int choice,category_priority,month,day,year;
    string strInput;
    
    do{
        cout<<"What piece would you like to edit?\n"
                <<"1  Name\n"
                <<"2  Description\n"
                <<"3  Category\n"
                <<"4  Priority\n"
                <<"5  Start Date\n"
                <<"6  Due Date\n"
                <<"7  Time required\n"
                <<"8  Resources needed\n"
                <<"9  Task list"
                <<"0  Completion Status\n"
                <<"-1 Return to main menu\n"
                <<">";
        cin>>choice;
        clearBuffer();
    }while(choice<0 || choice>9);
    
    switch(choice){
	case -1:
                cout<<"Returning to main menu\n\n";
                return;
	case 1:
                
                getName(strInput);
                nodeData.setName(strInput);
                break;
	case 2:
                getDesc(strInput);
                nodeData.setDesc(strInput);
                break;
	case 3:
                getCat(category_priority);
                nodeData.setCat(category_priority);
        	break;
	case 4:
                getPrio(category_priority);
                nodeData.setPrio(category_priority);
		break;
	case 5:
                getMonthDayYearStart(month,day,year);
                nodeData.setStartDate(month,day,year);
		break;
	case 6:
		getMonthDayYearDue(month,day,year);
                nodeData.setDueDate(month,day,year);
		break;
	case 7:
                getTime(category_priority);
                nodeData.setTime(category_priority);
		break;
	case 8:
                editResources(nodeData);
		break;
        case 9:
                editTasks(nodeData);
                break;
        case 0:
                getStatus(category_priority);
                nodeData.setStatus(category_priority);
                break;
    }
		
}//editWhatPart

 void editResources(Objective& nodeData){
     int choice;
     
     do{
         cout<<"\n\nWould you like to: \n"
                 <<"0 Return to main menu\n"
                 <<"1 Display Resources\n"
                 <<"2 Add a Resource\n"
                 <<"3 Delete a Resource (by position)\n"
                 <<"4 Delete a Resource (by name)\n"
                 <<">";
         cin>>choice;
         clearBuffer();

         string* l = nodeData.getRes();
         switch(choice){
             case 0:
                 cout<<"Returning to main menu\n\n";
             case 1:
                 for(int i=0;i<nodeData.getRUsed();i++) {
                    cout << l[i] << endl;
                 }
                 break;
             case 2:
                 addResource(nodeData);
                 break;
             case 3:
                 deleteResourcePos(nodeData);
                 break;
             case 4:
                 deleteResourceNam(nodeData);
                 break;
             default:
                 cout<<"\n\nInvalid Selection\n\n";
         }
             
     }while(choice!=0);   
     return;
             
}//editResources
 
void addResource(Objective& nodeData){
    string strInput;
    
    cout<<"\nAdd a resource that the objective needs: ";
    getline(cin,strInput);
    
    nodeData.addResource(strInput);
}//addResource
    
void deleteResourcePos(Objective& nodeData){
    int index;
    
    cout<<"What resource would you like to remove? (number):";
    cin>>index;
    
    nodeData.removeResource(index-1);
}//deleteResourcePos

void deleteResourceNam(Objective& nodeData){
    string name;
    
    cout<<"What resource would you like to remove?(name):";
    getline(cin,name);
    
    nodeData.removeResource(name);
}//deleteResourceNam

void editTasks(Objective& nodeData){
    int choice;
    string strInput;
    int numInput;
     
     do{
         cout<<"\n\nWould you like to: \n"
                 <<"0 Return to main menu\n"
                 <<"1 Display tasks\n"
                 <<"2 Add a task\n"
                 <<"3 Delete a task (by position)\n"
                 <<"4 Edit a task(by position)\n"
                 <<">";
         cin>>choice;
         clearBuffer();

         Cmpsc122Project::Task* l = nodeData.getTasks();
         switch(choice){
             case 0:
                 cout<<"Returning to main menu\n\n";
             case 1:
                 displayTaskNames(nodeData);
                 break;
             case 2:
                 getTasks(nodeData,strInput,numInput);
                 break;
             case 3:
                 deleteTaskPos(nodeData);
                 break;
             case 4:
                 editWhatTask(nodeData);
                 break;
             default:
                 cout<<"\n\nInvalid Selection\n\n";
         }
             
     }while(choice!=0);   
     return;
}//editTasks

void deleteTaskPos(Objective& nodeData){
    int numInput;
    
    displayTaskNames(nodeData);
    
    cout<<"\nWhat number task would you like to remove?: ";
    cin>>numInput;
    clearBuffer();
    
    nodeData.removeTask(numInput);
    
}//deleteTaskPos

void displayTaskNames(Objective& nodeData){
    int i=0;
    
    for(Cmpsc122Project::Task* temp= nodeData.getTasks(); temp!=NULL; temp=temp->next){
        cout<<i<<".) "<<temp->getName()<<"\n";
                
        i++;
    }
}//displayTasks

void editWhatTask(Objective& nodeData){
    int numInput;
    do{
        displayTaskNames(nodeData);

        cout<<"\nWhat number task would you like to edit? (0 to exit): ";
        cin>>numInput;
        clearBuffer();

        if(numInput>nodeData.getTasksSize() || numInput<0)
            cout<<"ERROR: Invalid Choice. Returning to selection";
        else if(numInput==0)
            return;
        else{
            Cmpsc122Project::Task*temp;
            
            for(int i=1;i!=numInput && temp!=NULL; i++,temp=temp->next)
                editTaskValues(*temp);
        }
    }while(true);
}//editWhatTask

void displayTasksInFull(Objective& nodeData){
    for(Cmpsc122Project::Task*current=nodeData.getTasks(); current!=NULL; current=current->next){
        displayTask(*current);
    }
    
}//displayTasksInFull

void displayTask(Task& task){
    
    cout<<"\nNAME: "<<task.getName()
            <<"\nDESCRIPTION: "<<task.getDesc()
            <<"\nSTART DATE: "<<task.getStartDate().toString()
            <<"\nDUE DATE: "<<task.getDueDate().toString()
            <<"\nTIME REQUIRED: "<<task.getTime()
            <<"\nRESOURCES: ";
            string* l = task.getRes();
            for(int i=0;i<task.getRUsed();i++) {
                cout << l[i] << endl;
            }
            cout<<"COMPLETED?: ";
    if(task.getStatus())
        cout<<"YES\n\n";
    else
        cout<<"NO\n\n";
    
}//displayTask

void editTaskValues(Task& task){

    int choice,category_priority,month,day,year;
    string strInput;

    do{
        cout<<"What piece would you like to edit?\n"
                <<"1  Name\n"
                <<"2  Description\n"
                <<"3  Start Date\n"
                <<"4  Due Date\n"
                <<"5  Time required\n"
                <<"6  Resources needed\n"
                <<"7  Completion Status\n"
                <<"0 Return to task menu\n"
                <<">";
        cin>>choice;
        clearBuffer();
    }while(choice<0 || choice>9);

    switch(choice){
    case 0:
        cout<<"Returning to task menu\n\n";
        return;
    case 1:
        cout<<"What is the name of the task?: ";
        getline(cin,strInput);
        task.setName(strInput);
        break;
    case 2:
        cout<<"What is the description for the task?: ";
        getline(cin,strInput);
        task.setDesc(strInput);
        break;
    case 3:
        getTaskMonthDayYearStart(month,day,year);
        task.setStartDate(month,day,year);
        break;
    case 4:
        getTaskMonthDayYearDue(month,day,year);
        task.setDueDate(month,day,year);
        break;
    case 5:
        getTime(category_priority);
        task.setTime(category_priority);
        break;
    case 6:
        editTaskResources(task);
        break;
    case 7:
        getStatus(category_priority);
        task.setStatus(category_priority);
        break;
    }
    return;
}//editTaskValues

void editTaskResources(Task& task){
     int choice,numInput;
     string strInput;
     
     do{
         cout<<"\n\nWould you like to: \n"
                 <<"0 Return to task menu\n"
                 <<"1 Display task resources\n"
                 <<"2 Add a task resource\n"
                 <<"3 Delete a task resource (by position)\n"
                 <<"4 Delete a task resource (by name)\n"
                 <<">";
         cin>>choice;
         clearBuffer();

         
         switch(choice){
             case 0:
                 cout<<"Returning to main menu\n\n";
             case 1:
                 displayTaskResources(task);
                 break;
             case 2:
                 getTaskResources(task);
                 break;
             case 3:
                 deleteTaskResourcePos(task);
                 break;
             case 4:
                 deleteTaskResourceNam(task);
                 break;
             default:
                 cout<<"\n\nInvalid Selection\n\n";
         }
             
     }while(choice!=0);   
     return;
             
}//editTaskResources

void displayTaskResources(Task& task){
    
    string* l = task.getRes();
    
    for(int i=0;i<task.getRUsed();i++) 
        cout <<i++<<".) "<< l[i] << endl;
    
}

void deleteTaskResourcePos(Task& task){
    int index;
    
    displayTaskResources(task);
    
    cout<<"What number resource would you like to remove?: ";
    cin>>index;
    
    task.removeResource(index-1);
}//deleteTaskResoursePos

void deleteTaskResourceNam(Task& task){
    string name;
    
    displayTaskResources(task);
    
    clearBuffer();
    cout<<"\nWhat resource would you like to remove?(name): ";
    getline(cin,name);
    
    task.removeResource(name);
}//deleteTaskResourseNam

void getFile(char filePath[]){
    cout<<"What is the file you want to use?: ";
    cin>>filePath;
            
}//getFile

void clearBuffer(){
    string clear;
    getline(cin,clear);
}

/*


 */
