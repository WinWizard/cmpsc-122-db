/* 
 * File:   Task.h
 * Author: Dylan
 *
 * Created on March 24, 2014, 9:30 AM
 */

#ifndef TASK_H
#define	TASK_H
#include "Date.h"
#include <string>
#define INIT_SIZE 1
using std::string;

namespace Cmpsc122Project {

    class Task {
    public:
        //constructors
        Task();
        Task(const Task& orig);
        ~Task();
        
        //operators
        Task& operator =(const Task& t1);
        friend bool operator ==(const Task& lhs, const Task& rhs);
        friend bool operator !=(const Task& lhs, const Task& rhs);
        void swap(const Task& orig);
        
        //accessor functions
        string getName() const; //gets the name of the task
        string getDesc() const; //gets the description of the task
        Date getDueDate() const; //returns the due date of the task
        Date getStartDate() const; //returns the number of elements in the resource list
        int getRUsed() const; //gets the amount of resources used
        int getTime() const; //gets the time required to finish (again type may change)
        string* getRes() const; //gets the list of resources (see resourcelist variable) //but how do i display it?-Zach
        bool getStatus() const; //gets the completion status
        int getResSize() const; //gets the size of the resource array
        
        //mutator functions
        void setName(string Name);
        void setDesc(string desc);
        void setStatus(bool stat); //sets status of completion to true if done, false if not
        void setTime(int t); // sets the time as an integer
        void setStartDate(int m, int d, int y); //sets start date from Date.h
        void setDueDate(int m, int d, int y); //Unsure what is needed for it at the moment -Ryan
        
        //resourceList Mutators
        bool addResource(string res); //adds a resource to the list
        bool removeResource(int index); //removes resource at index
        bool removeResource(string res); //removes resource that matches the string
        Task* getLink();
        bool setLink(const Task& t);
        bool setLink(Task* t);
        Task* next;
        
    private:
        //member variables
        string name; //task name
        string description; //task desc
        Date dueDate; //start date of the task
        Date startDate; //due date of the task
        int time; //time in hours required for completion
        bool isComplete; //status of completion of the objective

        //resourceList member variables
        string* resourceList; //list of needed resources using dynamic array (no Ryan it cannot be a vector)
        int listSize; //size of the dynamic array
        int listUsed; //amount of the dynamic array used
    };
}
#endif	/* TASK_H */

