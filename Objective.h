/* 
 * File:   Objective.h
 * Author: Dylan & Ryan
 *
 * Created on February 17, 2014, 2:56 PM
 */

#include <string>
#include "Date.h"
#include <fstream>
#include "Task.h"
#include "node1.h"
#ifndef OBJECTIVE_H
#define	OBJECTIVE_H
#define INIT_SIZE 1

using std::string;
using std::ifstream;
using std::ofstream;

namespace Cmpsc122Project {
/*
    struct Node { //structure for taskList node
        Task task;
        Node* next = NULL;
    };
*/
    class Objective {
    public:
        //constructors
        Objective();
        Objective(const Objective& orig);
        ~Objective();

        //operators
        Objective& operator =(const Objective& orig);
        friend bool operator ==(const Objective& lhs, const Objective& rhs);
        friend bool operator !=(const Objective& lhs, const Objective& rhs);
        void swap(const Objective& orig); //swaps two objectives

        //accessor functions
        string getName() const; //gets the name of the objective
        string getDesc() const; //gets the description of the objective
        int getPrio() const; //gets the priority of the objective
        string getCatAsString() const; //gets the category as a string
        int getCatAsInt() const; //gets the category as a numerical value
        Date getDueDate() const; //returns the due date of the objective
        Date getStartDate() const; //returns the start date of the objective
        int getRUsed() const; //returns the number of elements in the resource list
        int getTime() const; //gets the time required to finish (again type may change)
        string* getRes() const; //gets the list of resources (see resourcelist variable) //but how do i display it?-Zach
        Task* getTasks() const; //gets the list of tasks;
        bool getStatus() const; //gets the completion status of the objective
        int getResSize() const; //gets the size of the resource list
        int getTasksSize() const; //gets the size of the task list

        //mutator functions
        void setName(string Name);
        void setDesc(string desc);
        void setPrio(int prio);
        void setCat(int cat); //sets the category via int value (1-4)
        void setCat(string cat); //sets the category via its string name (Quad 1, Quad 2, Quad 3, Quad 4)
        void setStatus(bool stat); //sets status of completion to true if done, false if not
        void setTime(int t); // sets the time as an integer
        void setStartDate(int m, int d, int y); //sets start date from Date.h
        void setDueDate(int m, int d, int y); //Unsure what is needed for it at the moment -Ryan

        //mutators for resourceList
        bool addResource(string res); //adds a resource to the list
        bool removeResource(int index); //removes resource at index
        bool removeResource(string res); //removes resource that matches the string

        //mutators for taskList
        bool addTask(const Task& task); //adds a task to the list
        bool removeTask(int index); //removes task at index
        bool taskListClear();

        //File IO
        void readData(ifstream& filePath); //reads the data from a file
        void writeData(string filePath); //writes the data to a file
        void clearOutStream(string filename); //used to clear destination output file
        
        
          Objective* getLink();
        bool setLink(const Objective& t);
        bool setLink(Objective* t);

        Objective* next;

    private:
        //file io
        ofstream outData; //may or may not be needed here depends on how main is implemented

        //member variables
        string name; //objective name
        string description; //objective description
        int category; //Objective category (int from 1-4)
        int priority; //objective priority (int from 1-5)
        Date dueDate; //Date the objective is due from Date.h
        Date startDate; //Date the objective is assigned from Date.h
        int time; //time required to finish (type subject to change)
        bool isComplete; //status of completion of the objective

        //resource list variables
        string* resourceList; //list of needed resources using dynamic array (no Ryan it cannot be a vector)
        int listSize; //size of the dynamic array
        int listUsed; //amount of the dynamic array used

        //task list variables
        Task* taskList; //dynamic array for tasks
        int taskListSize; //dynamic array for tasks
        
        
    };
}
#endif	/* OBJECTIVE_H */

