/*********************************************************
 * file name: main.cpp
 * programmer name: Dylan Kozicki & Zachary McMinn & Ryan Ousey
 * date created: February 21, 2014
 * date of last revision: February 21, 2014
 * details of the revision: none
 * short description:  <enter description here>
 **********************************************************/




#include <iostream>
#include <string>
#include "Goal.h"
using namespace std;
using Cmpsc122Project::Goal;
using Cmpsc122Project::Objective;
using Cmpsc122Project::Task;

Goal* head;

//FUNCTION PROTOTYPES
void CreateGoal();
void CreateObjective(Objective&entry);
void CreateTask(Task&entry);
void EditGoal(int choice);
void displayAll();
void displayGoal(int choice);
void displayObjective(Objective* rootObjective);
void displayTask(Task* rootTask);
void clearBuffer();

int main() {
    Goal* current;
    head = NULL;
    int numInput;
    char choice;

    do {
        cout << "\n\n----------------ROOT MENU----------------\n"
                << "\n\tWhat would you like to do?\n"
                << "1  Add a Goal\n"
                << "2  Edit a Goal\n"
                << "3  Delete a Goal\n"
                << "4  Display a Goal\n"
                << "5  Display all Goals (abridged)\n"
                << "6  Write the list of Goals to a file\n"
                << "7  Retrieve a list of Goals from a file\n"
                << "0  Close the program\n"
                << ">";
        cin >> choice;
        clearBuffer();

        switch (choice) {
            case'1': //ADD A GOAL-----------------------------------------------------------------------


                CreateGoal();
                cout << "\nGoal*** added...\n";
                break;
            case'2': //EDIT A GOAL-----------------------------------------------------------------------
                if (head == NULL)
                    cout << "\nThere are no Goals to edit. Returning to Root Menu...\n";
                else {
                    cout << "\nWhat Goal would you like to edit?:\n";
                    displayAll();
                    cout << "-1.) Return to Root Menu\n"
                            << ">";
                    cin >> numInput;
                    clearBuffer();

                    if (numInput != -1)
                        EditGoal(numInput);
                    else if (numInput == -1)
                        cout << "\nReturning to Root Menu...\n";
                }
                break;
            case'3': //DELETE A GOAL-----------------------------------------------------------------------
                ;
                break;
            case'4': //DISPLAY A GOAL-----------------------------------------------------------------------
                if (head == NULL)
                    displayAll();
                else {
                    cout << "\nWhat Goal would you like to display?:\n";
                    displayAll();
                    cout << "\n>";
                    cin >> numInput;

                    Goal*current = head;
                    int i;
                    for (i = 1; i < numInput && current != NULL; ++i)
                        current = current->next;

                    displayGoal(i);
                    cout << "\n\nneed to add Tasks and Objectives...\n\n";
                }

                break;
            case'5': //DISPLAY ALL GOALS(abridged)-----------------------------------------------------------------------
                displayAll();
                break;
            case'6': //WRITE GOALS TO FILE-----------------------------------------------------------------------
                ;
                break;
            case'7': //READ GOALS FROM FILE-----------------------------------------------------------------------
                ;
                break;
            case'0': //END PROGRAM-----------------------------------------------------------------------
                cout << "\nTerminating Program...\n\n";

                break;
            default:
                cout << "\nINVALID CHOICE: Returning to Root Menu...\n";
        }
    } while (choice != '0');

    return 0;
}

void CreateGoal() {
    string input;
    char numInput;
    Goal *current = NULL;
    int numFinal, day, month, year;

    if (head == NULL) {
        head = new Goal;
        current = head;
    }
    else {
        current = head;

        while (current->next != NULL) {
            current = current->next;
        }
        current->setLink(new Goal());

        current = current->next;
    }

    //NAME-----------------------------------------------------------------------
    cout << "\nWhat is the name of the Goal?: ";
    getline(cin, input);
    current->setName(input);

    //DESC-----------------------------------------------------------------------
    cout << "\nWhat is the description of the Goal?: ";
    getline(cin, input);
    current->setDesc(input);

    //PRIORITY     --USES ISDIGIT-----------------------------------------------------------------------
    do {
        cout << "\nWhat numerical level of priority is the Goal?: ";
        cin>>numInput;
        clearBuffer();

        if (isdigit(numInput))
            numFinal = numInput - '0';
        else
            cout << "\nINVALID INPUT: Please give a number...\n";

    } while (!isdigit(numInput));
    current->setPrio(numFinal);

    //CATEGORY     --USES ISDIGIT-----------------------------------------------------------------------
    do {
        cout << "\nWhat category is the Goal?:\n"
                << "1 Urgent/Important\n"
                << "2 Urgent/Not Important\n"
                << "3 Not Urgent/Important\n"
                << "4 Not Urgent/Not Important\n"
                << ">";
        cin>>numInput;
        clearBuffer();

        if (numInput != '1' && numInput != '2' && numInput != '3' && numInput != '4')
            cout << "INVALID INPUT: Please select a number between 1 and 4";

    } while (numInput != '1' && numInput != '2' && numInput != '3' && numInput != '4');
    current->setPrio(numInput);

    //STATUS-----------------------------------------------------------------------
    do {
        cout << "\nIs the Goal completed?(Y/N): ";
        cin>>numInput;
        clearBuffer();

        if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
            cout << "\nINVALID INPUT: Please give a 'Y' if the Goal is completed\n"
                << "or 'N' if the goal is not completed\n";

    } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

    if (toupper(numInput) == 'Y')
        numFinal = true;
    else
        numFinal = false;

    current->setStatus(numFinal);

    //TIME     --USES ISDIGIT-----------------------------------------------------------------------
    do {
        cout << "\nHow many hours will the Goal take to complete? (0 if already completed): ";
        cin>>numInput;
        clearBuffer();

        if (!isdigit(numInput))
            cout << "\nINVALID INPUT: Please input a number...\n";

    } while (!isdigit(numInput));

    //START DATE     --USES ISDIGIT-----------------------------------------------------------------------
    //DAY
    do {
        cout << "\nWhat day did this Goal start: ";
        cin>>numFinal;

        if (numFinal < 1 || numFinal > 31)
            cout << "\nINVALID NUMBER: Please give a day between 1 and 31...\n";

    } while (numFinal < 1 || numFinal > 31);
    day = numFinal;

    //MONTH
    do {
        cout << "\nWhat month did this Goal start: ";
        cin>>numFinal;

        if (numFinal < 1 || numFinal > 12)
            cout << "\nINVALID NUMBER: Please give a month between 1 and 12...\n";

    } while (numFinal < 1 || numFinal > 12);
    month = numFinal;

    //YEAR
    do {
        cout << "\nWhat year did the Goal start: ";
        cin>>numFinal;

        if (numFinal < 2001)
            cout << "\nINVALID NUMBER: Please give a year after 2000...\n";

    } while (numFinal < 2001);
    year = numFinal;

    clearBuffer();
    current->setStartDate(month, day, year);

    //DUE DATE     --USES ISDIGIT-----------------------------------------------------------------------
    //DAY
    do {
        cout << "\nWhat day is this Goal due: ";
        cin>>numFinal;

        if (numFinal < 1 || numFinal > 31)
            cout << "\nINVALID NUMBER: Please give a day between 1 and 31...\n";

    } while (numFinal < 1 || numFinal > 31);
    day = numFinal;

    //MONTH
    do {
        cout << "\nWhat month is this Goal due: ";
        cin>>numFinal;

        if (numFinal < 1 || numFinal > 12)
            cout << "\nINVALID NUMBER: Please give a month between 1 and 12...\n";

    } while (numFinal < 1 || numFinal > 12);
    month = numFinal;

    //YEAR
    do {
        cout << "\nWhat year is the Goal due: ";
        cin>>numFinal;

        if (numFinal < 2001)
            cout << "\nINVALID NUMBER: Please give a year after 2000...\n";
        else if (numFinal < year)
            cout << "\nINVALID YEAR: Please choose a year equal or larger to the start year\n";

    } while (numFinal < 2001 || numFinal < year);
    year = numFinal;

    clearBuffer();
    current->setDueDate(month, day, year);

    //OBJECTIVES-----------------------------------------------------------------------
    do {
        cout << "\nWould you like to add an Objective to this Goal?(Y/N): ";
        cin>>numInput;
        clearBuffer();

        if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
            cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";
    } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

    if (toupper(numInput) == 'Y') {
        do {
            Objective obj_entry;
            CreateObjective(obj_entry);

            current->addObjective(obj_entry);

            do {
                cout << "\nWould you like to add an additional Objective to this Goal?(Y/N): ";
                cin>>numInput;
                clearBuffer();

                if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
                    cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";

            } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

            if (toupper(numInput) == 'N')
                cout << "\nAdditional Objectives will not be added...\n";

        } while (toupper(numInput) != 'N');
    } else {
        cout << "\nObjectives will not be added...\n";
        //current->makeNull();
    }

    current->setLink(NULL);

    cout << "\nGoal added...\n";


}//CreateGoal

void CreateObjective(Objective&entry) {
    string input;
    char numInput;
    int numFinal, day, month, year;

    //NAME-----------------------------------------------------------------------
    cout << "\nWhat is the name of the Objective?: ";
    getline(cin, input);
    entry.setName(input);

    //DESC-----------------------------------------------------------------------
    cout << "\nWhat is the description of the Objective?: ";
    getline(cin, input);
    entry.setDesc(input);

    //PRIORITY     --USES ISDIGIT-----------------------------------------------------------------------
    do {
        cout << "\nWhat numerical level of priority is the Objective?: ";
        cin >> numInput;
        clearBuffer();

        if (isdigit(numInput))
            numFinal = numInput - '0';
        else
            cout << "\nINVALID INPUT: Please give a number...\n";

    } while (!isdigit(numInput));
    entry.setPrio(numFinal);

    //CATEGORY     --USES ISDIGIT-----------------------------------------------------------------------
    do {
        cout << "\nWhat category is the Objective?:\n"
                << "1 Urgent/Important\n"
                << "2 Urgent/Not Important\n"
                << "3 Not Urgent/Important\n"
                << "4 Not Urgent/Not Important\n"
                << ">";
        cin >> numInput;
        clearBuffer();

        if (numInput != '1' && numInput != '2' && numInput != '3' && numInput != '4')
            cout << "INVALID INPUT: Please select a number between 1 and 4";

    } while (numInput != '1' && numInput != '2' && numInput != '3' && numInput != '4');
    entry.setPrio(numInput);

    //STATUS-----------------------------------------------------------------------
    do {
        cout << "\nIs the Objective completed?(Y/N): ";
        cin >> numInput;
        clearBuffer();

        if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
            cout << "\nINVALID INPUT: Please give a 'Y' if the Objective is completed\n"
                << "or 'N' if the Objective is not completed\n";

    } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

    if (toupper(numInput) == 'Y')
        numFinal = true;
    else
        numFinal = false;

    entry.setStatus(numFinal);

    //TIME     --USES ISDIGIT-----------------------------------------------------------------------
    do {
        cout << "\nHow many hours will the Objective take to complete? (0 if already completed): ";
        cin >> numInput;
        clearBuffer();

        if (!isdigit(numInput))
            cout << "\nINVALID INPUT: Please input a number...\n";

    } while (!isdigit(numInput));

    //START DATE     --USES ISDIGIT-----------------------------------------------------------------------
    //DAY
    do {
        cout << "\nWhat day did this Objective start: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 31)
            cout << "\nINVALID NUMBER: Please give a day between 1 and 31...\n";

    } while (numFinal < 1 || numFinal > 31);
    day = numFinal;

    //MONTH
    do {
        cout << "\nWhat month did this Objective start: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 12)
            cout << "\nINVALID NUMBER: Please give a month between 1 and 12...\n";

    } while (numFinal < 1 || numFinal > 12);
    month = numFinal;

    //YEAR
    do {
        cout << "\nWhat year did the Objective start: ";
        cin >> numFinal;

        if (numFinal < 2001)
            cout << "\nINVALID NUMBER: Please give a year after 2000...\n";

    } while (numFinal < 2001);
    year = numFinal;

    clearBuffer();
    entry.setStartDate(month, day, year);

    //DUE DATE     --USES ISDIGIT-----------------------------------------------------------------------
    //DAY
    do {
        cout << "\nWhat day is this Objective due: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 31)
            cout << "\nINVALID NUMBER: Please give a day between 1 and 31...\n";

    } while (numFinal < 1 || numFinal > 31);
    day = numFinal;

    //MONTH
    do {
        cout << "\nWhat month is this Objective due: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 12)
            cout << "\nINVALID NUMBER: Please give a month between 1 and 12...\n";

    } while (numFinal < 1 || numFinal > 12);
    month = numFinal;

    //YEAR
    do {
        cout << "\nWhat year is the Objective due: ";
        cin >> numFinal;

        if (numFinal < 2001)
            cout << "\nINVALID NUMBER: Please give a year after 2000...\n";
        else if (numFinal < year)
            cout << "\nINVALID YEAR: Please choose a year equal or larger to the start year\n";

    } while (numFinal < 2001 || numFinal < year);
    year = numFinal;

    clearBuffer();
    entry.setDueDate(month, day, year);

    //RESOURCES-----------------------------------------------------------------------
    do {
        cout << "\nWould you like to add resources to this Objective?(Y/N): ";
        cin >> numInput;
        clearBuffer();

        if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
            cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";
    } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

    if (toupper(numInput) == 'Y') {
        cout << "Resource: ";
        getline(cin, input);

        entry.addResource(input);

        do {//Loop for repeated resources
            do {//Loop to make sure input is valid
                cout << "\nWould you like to add additional resources to this Objective?(Y/N): ";
                cin >> numInput;
                clearBuffer();

                if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
                    cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";
            } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

            if (toupper(numInput) == 'Y') {
                cout << "\nResource: ";
                getline(cin, input);

                entry.addResource(input);

            } else
                cout << "\nAdditional resources will not be added...\n";
        } while (toupper(numInput) != 'N');
    } else
        cout << "\nResources will not be added...\n";

    //TASKS-----------------------------------------------------------------------
    do {
        cout << "\nWould you like to add Tasks to this Objective?(Y/N): ";
        cin >> numInput;
        clearBuffer();

        if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
            cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";
    } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

    if (toupper(numInput) == 'Y') {
        Task Tsk_entry;
        CreateTask(Tsk_entry);

        entry.addTask(Tsk_entry);

        do {//Loop for repeated tasks
            do {//Loop to make sure input is valid
                cout << "\nWould you like to add additional Tasks to this Objective?(Y/N): ";
                cin >> numInput;
                clearBuffer();

                if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
                    cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";
            } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

            if (toupper(numInput) == 'Y') {

                CreateTask(Tsk_entry);

                entry.addTask(Tsk_entry);
            } else
                cout << "\nAdditional Tasks will not be added...\n";
        } while (toupper(numInput != 'N'));
    } else
        cout << "\nTasks will not be added...\n";

    cout << "\nObjective added...\n";


}//CreateObjective

void CreateTask(Task&entry) {
    string input;
    char numInput;
    int numFinal, day, month, year;

    //NAME-----------------------------------------------------------------------
    cout << "\nWhat is the name of the Task?: ";
    getline(cin, input);
    entry.setName(input);

    //DESC-----------------------------------------------------------------------
    cout << "\nWhat is the description of the Task?: ";
    getline(cin, input);
    entry.setDesc(input);

    //STATUS-----------------------------------------------------------------------
    do {
        cout << "\nIs the Task completed?(Y/N): ";
        cin >> numInput;
        clearBuffer();

        if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
            cout << "\nINVALID INPUT: Please give a 'Y' if the Task is completed\n"
                << "or 'N' if the Objective is not completed\n";

    } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

    if (toupper(numInput) == 'Y')
        numFinal = true;
    else
        numFinal = false;

    entry.setStatus(numFinal);

    //TIME     --USES ISDIGIT-----------------------------------------------------------------------
    do {
        cout << "\nHow many hours will the Task take to complete? (0 if already completed): ";
        cin >> numInput;
        clearBuffer();

        if (!isdigit(numInput))
            cout << "\nINVALID INPUT: Please input a number...\n";

    } while (!isdigit(numInput));

    //START DATE     --USES ISDIGIT-----------------------------------------------------------------------
    //DAY
    do {
        cout << "\nWhat day did this Task start: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 31)
            cout << "\nINVALID NUMBER: Please give a day between 1 and 31...\n";

    } while (numFinal < 1 || numFinal > 31);
    day = numFinal;

    //MONTH
    do {
        cout << "\nWhat month did this Task start: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 12)
            cout << "\nINVALID NUMBER: Please give a month between 1 and 12...\n";

    } while (numFinal < 1 || numFinal > 12);
    month = numFinal;

    //YEAR
    do {
        cout << "\nWhat year did the Task start: ";
        cin >> numFinal;

        if (numFinal < 2001)
            cout << "\nINVALID NUMBER: Please give a year after 2000...\n";

    } while (numFinal < 2001);
    year = numFinal;

    clearBuffer();
    entry.setStartDate(month, day, year);

    //DUE DATE     --USES ISDIGIT-----------------------------------------------------------------------
    //DAY
    do {
        cout << "\nWhat day is this Task due: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 31)
            cout << "\nINVALID NUMBER: Please give a day between 1 and 31...\n";

    } while (numFinal < 1 || numFinal > 31);
    day = numFinal;

    //MONTH
    do {
        cout << "\nWhat month is this Task due: ";
        cin >> numFinal;

        if (numFinal < 1 || numFinal > 12)
            cout << "\nINVALID NUMBER: Please give a month between 1 and 12...\n";

    } while (numFinal < 1 || numFinal > 12);
    month = numFinal;

    //YEAR
    do {
        cout << "\nWhat year is the Task due: ";
        cin >> numFinal;

        if (numFinal < 2001)
            cout << "\nINVALID NUMBER: Please give a year after 2000...\n";
        else if (numFinal < year)
            cout << "\nINVALID YEAR: Please choose a year equal or larger to the start year\n";

    } while (numFinal < 2001 || numFinal < year);
    year = numFinal;

    clearBuffer();
    entry.setDueDate(month, day, year);

    //RESOURCES-----------------------------------------------------------------------
    do {
        cout << "\nWould you like to add resources to this Task?(Y/N): ";
        cin >> numInput;
        clearBuffer();

        if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
            cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";
    } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

    if (toupper(numInput) == 'Y') {
        cout << "Resource: ";
        getline(cin, input);

        entry.addResource(input);

        do {//Loop for repeated resources
            do {//Loop to make sure input is valid
                cout << "\nWould you like to add additional resources to this Task?(Y/N): ";
                cin >> numInput;
                clearBuffer();

                if (toupper(numInput) != 'Y' && toupper(numInput) != 'N')
                    cout << "\nINVALID INPUT: Please give 'Y' for yes or 'N' for no...\n";
            } while (toupper(numInput) != 'Y' && toupper(numInput) != 'N');

            if (toupper(numInput) == 'Y') {
                cout << "Resource: ";
                getline(cin, input);

                entry.addResource(input);

            } else
                cout << "\nAdditional resources will not be added...\n";
        } while (toupper(numInput != 'N'));
    } else
        cout << "\nResources will not be added...\n";

    cout << "\nTask added...\n";


}//CreateTask

void EditGoal(int choice) {
    Goal*current = head;
    int numInput;
    int i;
    for (i = 0; i < choice && current != NULL; i++) {
        current = current->next;

        if (current == NULL) {
            cout << "\nERROR: Invalid choice of Goal. Returning to Root Menu...\n";

        }//if
    }//for


    cout << "\nCurrently Editing:"
            << "\nGoal Name: ";

    displayGoal(i);

    cout << "\n\n\tWould you like to:"
            << "\n1 Edit the name of the Goal"
            << "\n2 Edit the description of the Goal"
            << "\n3 Edit the category of the Goal"
            << "\n4 Edit the priority of the Goal"
            << "\n5 Edit the start date of the Goal"
            << "\n6 Edit the due date of the Goal"
            << "\n7 Edit the amount of time the Goal will take"
            << "\n8 Edit the status of the Goal"
            << "\n9 Edit the Objectives of the Goal"\
			<< "\n-1 Return to Root Menu"
            << "\n>";
    cin >> numInput;
    clearBuffer();

    //UNFINISHED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}//EditGoal

void displayAll() {
    if (head == NULL)
        cout << "\nThe list is empty. Returning to root menu...\n";
    else {
        int i = 1;
        for (Goal* current = head; current != NULL; current = current->next) {
            cout << i << ".) " << current->getName() << endl;
            i++;
        }
    }
}//displayAll

void displayGoal(int choice) {
    Goal* current = NULL;
    current = head;
    int i;
    choice--;
    for (i = 0; i < choice && current != NULL; i++) {
        current = current->next;

        if (current == NULL) {
            cout << "\nERROR: Invalid choice of Goal. Returning to Root Menu...\n";

        }//if
    }//for
    cout << "Name: " << current->getName()
            << "\nDescription: - " << current->getDesc()
            << "\nStart Date: -- ";
    current->getStartDate();
    cout << "\nDue Date: ---- ";
    current->getDueDate();
    cout << "\nTime: -------- " << current->getTime()
            << "\nStatus: ------ " << current->getStatus();

    cout << "\nObjective List: \n";

    i = 1;
    Objective* o1 = current->getObjectives();
    if (o1 != NULL) {
        for (; o1->next != NULL; o1 = o1->next){
            cout << i << ".) ";
            displayObjective(o1);
            cout<<endl;
            i++;
        }


    }
}//displayGoal

void displayObjective(Objective* rootObjective) {
    
    cout << "Name: " << rootObjective->getName()
            << "\nDescription: - " << rootObjective->getDesc()
            << "\nStart Date: -- ";
    rootObjective->getStartDate();
    cout << "\nDue Date: ---- ";
    rootObjective->getDueDate();
    cout << "\nTime: -------- " << rootObjective->getTime()
            << "\nStatus: ------ " << rootObjective->getStatus();

    cout << "\nTask List: \n";
    
    int i = 1;
    Task* t1 = rootObjective->getTasks();
    if (t1 != NULL) {
        for (; t1->next != NULL; t1 = t1->next){
            cout << i << ".) ";
            displayTask(t1);
            cout<<endl;
            i++;
        }
        
    }

}//displayObjective

void displayTask(Task* rootTask) {
    cout << "Name: " << rootTask->getName()
            << "\nDescription: - " << rootTask->getDesc()
            << "\nStart Date: -- ";
    rootTask->getStartDate();
    cout << "\nDue Date: ---- ";
    rootTask->getDueDate();
    cout << "\nTime: -------- " << rootTask->getTime()
            << "\nStatus: ------ " << rootTask->getStatus();
}//displayTask

void clearBuffer() {
    string clear;
    getline(cin, clear);

}
