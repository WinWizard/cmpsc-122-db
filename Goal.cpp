/*********************************************************
 * file name: Goal.cpp
 * programmer name: Dylan Kozicki
 * date created: April 6, 2014
 * date of last revision: April 6, 2014
 * details of the revision: none
 * short description:  
 **********************************************************/

#include "Goal.h"
#include "Goal.h"
#include <new>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include"node1.h"

using std::cout;
using std::nothrow;

using std::transform;
using std::getline;
namespace Cmpsc122Project {
    //constructors
    Goal::Goal() {
        next = NULL;

        objectiveList = NULL;
    }

    Goal::~Goal() { //destructor
        
		objectiveListClear();
    }

    Goal::Goal(const Goal& orig) {//copy contructor
        swap(orig);
    }

    //operators
    Goal& Goal::operator =(const Goal& orig) {
        swap(orig);
        
        return *this;
    }

    bool operator ==(const Goal& lhs, const Goal& rhs) {
        if (lhs.getName().compare(rhs.getName())) {
            return false;
        }
        if (lhs.getDesc().compare(rhs.getName())) {
            return false;
        }
        if (lhs.getPrio() != rhs.getPrio()) {
            return false;
        }
        if (lhs.getCatAsInt() != rhs.getCatAsInt()) {
            return false;
        }
        if (lhs.getStatus() != rhs.getStatus()) {
            return false;
        }
        if (lhs.getDueDate() != rhs.getDueDate()) {
            return false;
        }
        if (lhs.getStartDate() != rhs.getStartDate()) {
            return false;
        }
        if (lhs.getTime() != rhs.getTime()) {
            return false;
        }

        //resourceList check
       

        //Objective List Check
        if (lhs.getObjectivesSize() != rhs.getObjectivesSize()) {
            return false;
        }
        Objective *left = lhs.getObjectives(), *right = rhs.getObjectives();
        do {
            if (*left != *right) {
                return false;
            }
            left = left->next;
            right = right->next;
            if (left->next == NULL || right->next == NULL) {
                if (*left != *right) {
                    return false;
                }
            }
        } while (left->next != NULL && right->next != NULL);

        return true;
    }

    bool operator !=(const Goal& lhs, const Goal& rhs) {
        return !(lhs == rhs);
    }
    
	void Goal::swap(const Goal& orig){
		//copy all non dynamic values
		setName(orig.getName());
		setDesc(orig.getDesc());
		setPrio(orig.getPrio());
		setCat(orig.getCatAsInt());
		setStatus(orig.getStatus());
		setTime(orig.getTime());
		setStartDate(orig.getStartDate().getMonth(), orig.getStartDate().getDay(), orig.getStartDate().getYear());
		setDueDate(orig.getDueDate().getMonth(), orig.getDueDate().getDay(), orig.getDueDate().getYear());

		
	
	if(objectiveList != NULL){
		objectiveListClear();
	}
        
        Objective* tmp = orig.getObjectives();
        do {
            addObjective(*tmp);
            tmp = tmp->next;
           
        } while (tmp!= NULL);

        //clear temp values
        tmp = NULL;
    }

    //accessor functions
    string Goal::getName() const {
        return name;
    }

    string Goal::getDesc() const {
        return description;
    }

    int Goal::getPrio() const {
        return priority;
    }

    string Goal::getCatAsString() const {
        switch (category) {//int initializes as 0 so not being set first is fine
            case 1:
                return "Quad 1";
                break;
            case 2:
                return "Quad 2";
                break;
            case 3:
                return "Quad 3";
                break;
            case 4:
                return "Quad 4";
                break;
            default:
                return "Invalid Quad";
                break;
        }
    }

    int Goal::getCatAsInt() const {
        return category;
    }

    Date Goal::getDueDate() const {
        return dueDate;
    }

    Date Goal::getStartDate() const {
        return startDate;
    }

   

    int Goal::getTime() const {
        return time;
    }



    Objective* Goal::getObjectives() const {
        return objectiveList;
    }

    bool Goal::getStatus() const {
        return isComplete;
    }



    int Goal::getObjectivesSize() const {
        return objectiveListSize;
    }

    //mutator functions
    void Goal::setName(string Name) {
        name = Name;
    }

    void Goal::setDesc(string desc) {
        description = desc;
    }

    void Goal::setPrio(int prio) {//priority is 1-5 if within range, otherwise set to 0
        if (prio > 0 && prio < 6)
            priority = prio;
        else
            priority = 0;
    }

    void Goal::setCat(int cat) {
        if (cat > 0 && cat < 5) // In range becomes cat, otherwise 0
            category = cat;
        else
            category = 0; // Get cat as string will handle this as "Invalid Quad"
    }

    void Goal::setCat(string cat) {

        transform(cat.begin(), cat.end(), cat.begin(), ::tolower); //makes cat all lowercase

        if (!cat.compare("quad 1")) {
            category = 1;
        }
        else if (!cat.compare("quad 2")) {
            category = 2;
        }
        else if (!cat.compare("quad 3")) {
            category = 3;
        }
        if (!cat.compare("quad 4")) {
            category = 4;
        }
        else {
            category = 0;
        }
    }

    void Goal::setStatus(bool stat) {
        isComplete = stat;
    }

    void Goal::setTime(int t) {
        time = t;
    }

    void Goal::setStartDate(int m, int d, int y) {
        startDate.setYear(y);
        startDate.setMonth(m);
        startDate.setDay(d);
    }

    void Goal::setDueDate(int m, int d, int y) {
        dueDate.setYear(y);
        dueDate.setMonth(m);
        dueDate.setDay(d);
    }

   

    //mutators for objectiveList

    bool Goal::addObjective(const Objective& objective) {

        

        if (objectiveList == NULL) {
            *objectiveList = objective;
            cout << "test";
            objectiveListSize = 1;
            return true;
        }
        else {
            Objective* temp = objectiveList;
            
            while (temp->next != NULL) {
                temp = temp->next;
                objectiveListSize++;
            }

            temp->next->setLink(objective);
            objectiveListSize++;

            temp = NULL;

            return true;
        }
    }

    bool Goal::removeObjective(int index) {
        Objective* temp = objectiveList;

        if (temp == NULL || index > objectiveListSize) {
            return false;
        }

        for (int i = 1; i < objectiveListSize && temp != NULL; i++) {
            if (i == index && i < objectiveListSize - 1) {


				temp->next->setLink(temp->next->next);
				temp = NULL;
            }
			else if (i == index && i == objectiveListSize){
				temp->setLink(NULL);
				
			}
			
				temp = temp->next;
			
        }

        return true;
    }
	bool Goal::objectiveListClear(){
		Objective* temp = objectiveList;
		while (temp != NULL){
			objectiveList = objectiveList->next;
			delete temp;

			temp = objectiveList;
		}
		objectiveList = NULL;
		return true;
	}

    //File IO

    void Goal::readData(ifstream& inData) {//Each piece of data must be on its own line -Ryan
    /*    if (inData.fail())//if fails prints to screen it cannot be found
            cout << "Cannot find input file";
        else {
            int tempInt, i2, i3; //to hold int for time and priority
            string tempStr; //to hold and read in data
            Objective *tempNode;
            getline(inData, tempStr);
            setName(tempStr);
            getline(inData, tempStr);

            setCat(tempStr);
            getline(inData, tempStr);
            setStatus((tempStr == "true" ? true : false)); //turns true string into bool, you can write anything tha isn't true to make false
            getline(inData, tempStr);

            setDesc(tempStr);
            inData >> tempInt;
            setTime(tempInt);
            //std::getline(inFile,tempStr);
            inData >> tempInt;
            setPrio(tempInt);
            //std::getline(inFile,tempStr);
            inData >> tempInt >> i2>>i3;

            setStartDate(tempInt, i2, i3); //month day year Ex.3 14 2014
            inData >> tempInt >> i2>>i3;
            setDueDate(tempInt, i2, i3);
            inData >> tempInt;
            getline(inData, tempStr);
            for (int i = 0; i < tempInt; i++) //goes through the next (int) lines for resources
            {
                getline(inData, tempStr);
                addResource(tempStr);
            }
            inData >> tempInt;
            getline(inData, tempStr);
            for (int i = tempInt; i > 0; i--) //goes through reading in objectives
            {
                Objective tempTsk;
                getline(inData, tempStr);
                tempTsk.setName(tempStr);
                getline(inData, tempStr);
                tempTsk.setStatus((tempStr == "true" ? true : false));
                getline(inData, tempStr);
                tempTsk.setDesc(tempStr);
                inData >> tempInt >> i2>>i3;
                tempTsk.setStartDate(tempInt, i2, i3);
                inData >> tempInt >> i2>>i3;
                tempTsk.setDueDate(tempInt, i2, i3);
                for (int i = 0; i < tempInt; i++) //goes through the next (int) lines for resources
                {
                    getline(inData, tempStr);
                    tempTsk.addResource(tempStr);
                }
                addObjective(tempTsk);
     
            }
        }*/
    }

    void Goal::writeData(string filePath) {
        
    }

    void Goal::clearOutStream(string filename) {
        
    }
    Goal* Goal::getLink(){
		if (next == NULL){
			return NULL;
		}
		return next;

	}
	bool Goal::setLink(const Goal& t){
		next = new Goal(t);
		return true;
	}
	bool Goal::setLink(Goal* t){
		next = t;
		return true;
	}
}


