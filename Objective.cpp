/*********************************************************
 * file name: Objective.cpp
 * programmer name: Dylan Kozicki & Ryan Ousey
 * date created: February 17, 2014
 * date of last revision: February 17, 2014
 * details of the revision: none
 * short description:  <enter description here>
 **********************************************************/

#include "Objective.h"
#include <new>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include"node1.h"

using std::cout;
using std::nothrow;

using std::transform;
using std::getline;
namespace Cmpsc122Project {
    //constructors
    Objective::Objective() {
        next = NULL;
		resourceList = NULL;
        resourceList = new (nothrow) string[INIT_SIZE]; //create resource list
        if (resourceList == NULL) { //check if the memory was allocated
            cout << "Error memory may not have been allocated";
        }
        
        listSize = INIT_SIZE;
        listUsed = 0;

        taskList = NULL;
    }

    Objective::~Objective() { //destructor
        delete[] resourceList;
        
		taskListClear();
		resourceList = NULL;
    }

    Objective::Objective(const Objective& orig) {//copy contructor
        swap(orig);
    }

    //operators
    Objective& Objective::operator =(const Objective& orig) {
        swap(orig);
        
        return *this;
    }

    bool operator ==(const Objective& lhs, const Objective& rhs) {
        if (lhs.getName().compare(rhs.getName())) {
            return false;
        }
        if (lhs.getDesc().compare(rhs.getName())) {
            return false;
        }
        if (lhs.getPrio() != rhs.getPrio()) {
            return false;
        }
        if (lhs.getCatAsInt() != rhs.getCatAsInt()) {
            return false;
        }
        if (lhs.getStatus() != rhs.getStatus()) {
            return false;
        }
        if (lhs.getDueDate() != rhs.getDueDate()) {
            return false;
        }
        if (lhs.getStartDate() != rhs.getStartDate()) {
            return false;
        }
        if (lhs.getTime() != rhs.getTime()) {
            return false;
        }

        //resourceList check
        if (lhs.getRUsed() != rhs.getRUsed()) {
            return false;
        }
        for (int i = 0; i < rhs.getRUsed(); i++) {
            if (lhs.getRes()[i] != rhs.getRes()[i]) {
                return false;
            }
        }

        //Task List Check
        if (lhs.getTasksSize() != rhs.getTasksSize()) {
            return false;
        }
        Task *left = lhs.getTasks(), *right = rhs.getTasks();
        do {
            if (*left != *right) {
                return false;
            }
            left = left->next;
            right = right->next;
            if (left->next == NULL || right->next == NULL) {
                if (*left != *right) {
                    return false;
                }
            }
        } while (left->next != NULL && right->next != NULL);

        return true;
    }

    bool operator !=(const Objective& lhs, const Objective& rhs) {
        return !(lhs == rhs);
    }
    
	void Objective::swap(const Objective& orig){
		//copy all non dynamic values
		setName(orig.getName());
		setDesc(orig.getDesc());
		setPrio(orig.getPrio());
		setCat(orig.getCatAsInt());
		setStatus(orig.getStatus());
		setTime(orig.getTime());
		setStartDate(orig.getStartDate().getMonth(), orig.getStartDate().getDay(), orig.getStartDate().getYear());
		setDueDate(orig.getDueDate().getMonth(), orig.getDueDate().getDay(), orig.getDueDate().getYear());

		//destroy old values
		//delete[] resourceList;
		resourceList = NULL;
	
	if(taskList != NULL){
		taskListClear();
	}
        //copy resource list
        resourceList = new (nothrow) string[orig.getResSize()];
        if (resourceList == NULL) {
            cout << "Error memory may not have been allocated";
        }
        listSize = orig.getResSize();
        listUsed = 0;

        //copy task list
        string* temp = orig.getRes();
		for (; listUsed < orig.getRUsed(); addResource(temp[listUsed]), listUsed++);

        cout << resourceList[0];
        Task* tmp = orig.getTasks();
        do {
            addTask(*tmp);
            tmp = tmp->next;
           
        } while (tmp!= NULL);

        //clear temp values
        tmp = NULL;
        temp = NULL;
    }

    //accessor functions
    string Objective::getName() const {
        return name;
    }

    string Objective::getDesc() const {
        return description;
    }

    int Objective::getPrio() const {
        return priority;
    }

    string Objective::getCatAsString() const {
        switch (category) {//int initializes as 0 so not being set first is fine
            case 1:
                return "Quad 1";
                break;
            case 2:
                return "Quad 2";
                break;
            case 3:
                return "Quad 3";
                break;
            case 4:
                return "Quad 4";
                break;
            default:
                return "Invalid Quad";
                break;
        }
    }

    int Objective::getCatAsInt() const {
        return category;
    }

    Date Objective::getDueDate() const {
        return dueDate;
    }

    Date Objective::getStartDate() const {
        return startDate;
    }

    int Objective::getRUsed() const {
        return listUsed;
    }

    int Objective::getTime() const {
        return time;
    }

    string* Objective::getRes() const {
        return resourceList;
    }

    Task* Objective::getTasks() const {
        return taskList;
    }

    bool Objective::getStatus() const {
        return isComplete;
    }

    int Objective::getResSize() const {
        return listSize;
    }

    int Objective::getTasksSize() const {
        return taskListSize;
    }

    //mutator functions
    void Objective::setName(string Name) {
        name = Name;
    }

    void Objective::setDesc(string desc) {
        description = desc;
    }

    void Objective::setPrio(int prio) {//priority is 1-5 if within range, otherwise set to 0
        if (prio > 0 && prio < 6)
            priority = prio;
        else
            priority = 0;
    }

    void Objective::setCat(int cat) {
        if (cat > 0 && cat < 5) // In range becomes cat, otherwise 0
            category = cat;
        else
            category = 0; // Get cat as string will handle this as "Invalid Quad"
    }

    void Objective::setCat(string cat) {

        transform(cat.begin(), cat.end(), cat.begin(), ::tolower); //makes cat all lowercase

        if (!cat.compare("quad 1")) {
            category = 1;
        }
        else if (!cat.compare("quad 2")) {
            category = 2;
        }
        else if (!cat.compare("quad 3")) {
            category = 3;
        }
        if (!cat.compare("quad 4")) {
            category = 4;
        }
        else {
            category = 0;
        }
    }

    void Objective::setStatus(bool stat) {
        isComplete = stat;
    }

    void Objective::setTime(int t) {
        time = t;
    }

    void Objective::setStartDate(int m, int d, int y) {
        startDate.setYear(y);
        startDate.setMonth(m);
        startDate.setDay(d);
    }

    void Objective::setDueDate(int m, int d, int y) {
        dueDate.setYear(y);
        dueDate.setMonth(m);
        dueDate.setDay(d);
    }

    //mutators for resourceList
    bool Objective::addResource(string res) {
        if (listUsed == listSize) { //determine if array is fully used
            string *temp = NULL; // create a pointer for a temporary array

            temp = new (nothrow) string[listSize + 1]; //create a temporary array of size (listSize+1)

            if (temp == NULL) { //make sure memory was allocated properly
                cout << "Error: memory could not be allocated";
                return false;
            }

			for (int i = 0; i < listUsed;temp[i] = resourceList[i], i++);
            delete[] resourceList; //delete the old list
            resourceList = temp; //Point resourceList to temp;

            listSize++;
        }

        resourceList[listUsed] = res; //add the resource to the list
        listUsed++;

        return true;
    }

    bool Objective::removeResource(int index) {
        string *temp = NULL; // create a pointer for a temporary array

        temp = new (nothrow) string[listSize - 1]; //create a temporary array of size (listSize-1)

        if (temp == NULL) { //make sure memory was allocated properly
            cout << "Error: memory could not be allocated";
            return false;
        }

        listSize--;
        listUsed--;

        for (int i = 0, j = 0; i <= listSize, j < listSize; i++) {
            if (i != index) { //check that the index is not the location of the object to be removed
                temp[j] = resourceList[i]; //set temp to the object in resourceList
                j++; //increment j only if the index is not the one which we are removing
            }
        }

        delete[] resourceList; //delete the old array

        resourceList = temp; //change where resourceList points to

        return true;
    }

    bool Objective::removeResource(string res) {
        int i = 0;
        bool isFound;

        for (; i < listSize && !isFound; isFound = !res.compare(resourceList[i]), i++); //search for the string in the array

        if (isFound) {

            return removeResource(i); //if it is found remove it 
        }
        else {
            cout << "Error. Chore not found.\n";
            return false;
        }
    }

    //mutators for taskList

    bool Objective::addTask(const Task& task) {

        

        if (taskList == NULL) {
            *taskList = task;
            cout << "test";
            taskListSize = 1;
            return true;
        }
        else {
            Task* temp = taskList;
            
            while (temp->next != NULL) {
                temp = temp->next;
                taskListSize++;
            }

            temp->next->setLink(task);
            taskListSize++;

            temp = NULL;

            return true;
        }
    }

    bool Objective::removeTask(int index) {
        Task* temp = taskList;

        if (temp == NULL || index > taskListSize) {
            return false;
        }

        for (int i = 1; i < taskListSize && temp != NULL; i++) {
            if (i == index && i < taskListSize - 1) {


				temp->next->setLink(temp->next->next);
				temp = NULL;
            }
			else if (i == index && i == taskListSize){
				temp->setLink(NULL);
				
			}
			
				temp = temp->next;
			
        }

        return true;
    }
	bool Objective::taskListClear(){
		Task* temp = taskList;
		while (temp != NULL){
			taskList = taskList->next;
			delete temp;

			temp = taskList;
		}
		taskList = NULL;
		return true;
	}

    //File IO

    void Objective::readData(ifstream& inData) {//Each piece of data must be on its own line -Ryan
        if (inData.fail())//if fails prints to screen it cannot be found
            cout << "Cannot find input file";
        else {
            int tempInt, i2, i3; //to hold int for time and priority
            string tempStr; //to hold and read in data
            Task *tempNode;
            getline(inData, tempStr);
            setName(tempStr);
            getline(inData, tempStr);

            setCat(tempStr);
            getline(inData, tempStr);
            setStatus((tempStr == "true" ? true : false)); //turns true string into bool, you can write anything tha isn't true to make false
            getline(inData, tempStr);

            setDesc(tempStr);
            inData >> tempInt;
            setTime(tempInt);
            //std::getline(inFile,tempStr);
            inData >> tempInt;
            setPrio(tempInt);
            //std::getline(inFile,tempStr);
            inData >> tempInt >> i2>>i3;

            setStartDate(tempInt, i2, i3); //month day year Ex.3 14 2014
            inData >> tempInt >> i2>>i3;
            setDueDate(tempInt, i2, i3);
            inData >> tempInt;
            getline(inData, tempStr);
            for (int i = 0; i < tempInt; i++) //goes through the next (int) lines for resources
            {
                getline(inData, tempStr);
                addResource(tempStr);
            }
            inData >> tempInt;
            getline(inData, tempStr);
            for (int i = tempInt; i > 0; i--) //goes through reading in tasks
            {
                Task tempTsk;
                getline(inData, tempStr);
                tempTsk.setName(tempStr);
                getline(inData, tempStr);
                tempTsk.setStatus((tempStr == "true" ? true : false));
                getline(inData, tempStr);
                tempTsk.setDesc(tempStr);
                inData >> tempInt >> i2>>i3;
                tempTsk.setStartDate(tempInt, i2, i3);
                inData >> tempInt >> i2>>i3;
                tempTsk.setDueDate(tempInt, i2, i3);
                for (int i = 0; i < tempInt; i++) //goes through the next (int) lines for resources
                {
                    getline(inData, tempStr);
                    tempTsk.addResource(tempStr);
                }
                addTask(tempTsk);
            }
        }
    }

    void Objective::writeData(string filePath) {
        outData.open(filePath, std::ios::app);
        if (outData.fail())
            cout << "Cannot find output file";
        else {
            outData << name << std::endl;
            outData << getCatAsString() << std::endl; //must get as string
            outData << (isComplete == true ? "true" : "false") << std::endl; //to convert bool to string
            outData << description << std::endl;
            outData << time << std::endl;
            outData << priority << std::endl;
            outData << startDate.getDay() << " " << startDate.getMonth() << " " << startDate.getYear() << std::endl;
            outData << dueDate.getDay() << " " << dueDate.getMonth() << " " << dueDate.getYear() << std::endl;
            for (int i = 0; i < listSize; i++)//runs through the list, last one doesn't have a newline after it
            {
                outData << resourceList[i] << "\n";
            }
             Task* temp = taskList;
            for(int i = taskListSize; i > 0; i--){
                outData << temp->getName() << std::endl;
                outData << (temp->getStatus() == true ? "true" : "false") << std::endl; //to convert bool to string
                outData << temp->getDesc() << std::endl;
                outData << temp->getTime() << std::endl;
                outData << temp->getStartDate().getDay() << " " << temp->getStartDate().getMonth() << " " << temp->getStartDate().getYear() << std::endl;
                outData << temp->getDueDate().getDay() << " " << temp->getDueDate().getMonth() << " " << temp->getDueDate().getYear() << std::endl;
                string* temp2 = temp->getRes();
                for (int i = 0; i < listSize; i++)//runs through the list, last one doesn't have a newline after it
                {
                    outData << temp2[i] << "\n";
                }
            }
        }
        outData.close();
    }

    void Objective::clearOutStream(string filename) {
        outData.open(filename);
        outData.close();
    }
    Objective* Objective::getLink(){
		if (next == NULL){
			return NULL;
		}
		return next;

	}
	bool Objective::setLink(const Objective& t){
		next = new Objective(t);
		return true;
	}
	bool Objective::setLink(Objective* t){
		next = t;
		return true;
	}
}



