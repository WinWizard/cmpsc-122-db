/* 
 * File:   Date.h
 * Author: Dylan
 *
 * Created on February 17, 2014, 3:19 PM
 */

#ifndef DATE_H
#define	DATE_H
#include <string>

using std::string;

namespace Cmpsc122Project {

    struct Month {
        string name;
        int days;
    };

    class Date {
    public:

        Date(int month, int day, int year); //date constructor requires a valid month day and year
        Date();

        int getMonth() const; //returns the month
        int getDay() const; //returns the day
        int getYear() const; //returns the year

        void setMonth(int m); //sets the month and the string value(requires an int month where 0<month<=12)
        void setDay(int day); //sets the day (requires an int day where 0<day<=31)
        void setYear(int year); //sets the year (requires a year greater than 0)

        string toString(); //prints outputs a string version of the date as follows "<month> <day>, <year>"

        friend bool operator ==(const Date& lhs, const Date& rhs);
        friend bool operator !=(const Date& lhs, const Date& rhs);

        Date(const Date& orig); //copy constructor
        ~Date();
    private:

        int date[3]; //array for the date where 0 is the month, 1 is the day, and 2 is the year
        Month month; //holder for the string value of the month for efficiency

    };
    static Month MONTHS[12] = {//string values for months

        {"January", 31},
        {"February", 28},
        {"March", 31},
        {"April", 30},
        {"May", 31},
        {"June", 30},
        {"July", 31},
        {"August", 31},
        {"September", 30},
        {"October", 31},
        {"November", 30},
        {"December", 31}
    };
}
#endif	/* DATE_H */
