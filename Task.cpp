/*********************************************************
 * file name: Task.cpp
 * programmer name: Dylan Kozicki
 * date created: March 24, 2014
 * date of last revision: March 24, 2014
 * details of the revision: none
 * short description:  <enter description here>
 **********************************************************/

#include "Task.h"
#include <iostream>
#include <cstdlib>

using std::nothrow;
using std::cout;
namespace Cmpsc122Project {
    //constructors
    Task::Task() {
		resourceList = NULL;

       resourceList = new (nothrow) string[INIT_SIZE]; //create resource list

		next = NULL;

        if (resourceList == NULL) { //check if the memory was allocated
            cout << "Error memory may not have been allocated";
        }

        listSize = INIT_SIZE;
        listUsed = 0;
    }

    Task::Task(const Task& orig) {
        swap(orig);
    }

	Task::~Task() {

		delete[] resourceList;
		resourceList = NULL;

	}
    //operators
    Task& Task::operator =(const Task& orig) {
        swap(orig);

        return *this;

    }

    bool operator ==(const Task& lhs, const Task& rhs) {
        if (lhs.getName().compare(rhs.getName())) {
            return false;
        }
        if (lhs.getDesc().compare(rhs.getName())) {
            return false;
        }
        if (lhs.getStatus() != rhs.getStatus()) {
            return false;
        }
        if (lhs.getDueDate() != rhs.getDueDate()) {
            return false;
        }
        if (lhs.getStartDate() != rhs.getStartDate()) {
            return false;
        }
        if (lhs.getTime() != rhs.getTime()) {
            return false;
        }
        if (lhs.getRUsed() != rhs.getRUsed()) {
            return false;
        }
        for (int i = 0; i < rhs.getRUsed(); i++) {
            if (lhs.getRes()[i] != rhs.getRes()[i]) {
                return false;
            }
        }

        return true;
    }

    bool operator !=(const Task& lhs, const Task& rhs) {
        return !(lhs == rhs);
    }

	void Task::swap(const Task& orig) {
		//swap non dynamic values
		setName(orig.getName());
		setDesc(orig.getDesc());
		setStatus(orig.getStatus());
		setTime(orig.getTime());
		setStartDate(orig.getStartDate().getMonth(), orig.getStartDate().getDay(), orig.getStartDate().getYear());
		setDueDate(orig.getDueDate().getMonth(), orig.getDueDate().getDay(), orig.getDueDate().getYear());

		//delete original values
		if (resourceList != NULL){
		//delete[] resourceList;
		resourceList = NULL;
	}
        //swap resourceList values
        resourceList = new (nothrow) string[orig.getResSize()];

        if (resourceList == NULL) {
            cout << "Error memory may not have been allocated";

        }

        listSize = orig.getResSize();
        listUsed = 0;

        string* temp = orig.getRes();
		cout << "DAFAQ";
		for (; listUsed < orig.getRUsed(); addResource(temp[listUsed]), listUsed++);
    }
    
    //accessor functions
    string Task::getName() const {
        return name;
    }

    string Task::getDesc() const {
        return description;
    }

    Date Task::getDueDate() const {
        return dueDate;
    }

    Date Task::getStartDate() const {
        return startDate;
    }

    int Task::getRUsed() const {
        return listUsed;
    }
    
    int Task::getTime() const {
        return time;
    }
    
    int Task::getResSize() const {
        return listSize;
    }
   
    bool Task::getStatus() const {
        return isComplete;
    }

    string* Task::getRes() const {
        return resourceList;
    }
    
    //Mutator functions
    void Task::setName(string Name) {
        name = Name;
    }

    void Task::setDesc(string desc) {
        description = desc;
    }
    
    void Task::setStatus(bool stat) {
        isComplete = stat;
    }
    
    void Task::setTime(int t) {
        time = t;
    }

    void Task::setStartDate(int m, int d, int y) {
        startDate.setYear(y);
        startDate.setMonth(m);
        startDate.setDay(d);
    }
    
    void Task::setDueDate(int m, int d, int y) {
        dueDate.setYear(y);
        dueDate.setMonth(m);
        dueDate.setDay(d);
    }

    //resourceList mutators
    bool Task::addResource(string res) {
        if (listUsed == listSize) { //determine if array is fully used
            string *temp = NULL; // create a pointer for a temporary array

            temp = new (nothrow) string[listSize + 1]; //create a temporary array of size (listSize+1)
            if (temp == NULL) { //make sure memory was allocated properly
                cout << "Error: memory could not be allocated";
                return false;
            }

            std::copy(resourceList, resourceList + listSize, temp); //copy the old list to the new one
			if (resourceList != NULL){
				delete[] resourceList; //delete the old list
			}
			resourceList = temp; //Point resourceList to temp;

            listSize++;
        }

        resourceList[listUsed] = res; //add the resource to the list
        listUsed++;

        return true;
    }

    bool Task::removeResource(int index) {
        string *temp = NULL; // create a pointer for a temporary array
       
        temp = new (nothrow) string[listSize - 1]; //create a temporary array of size (listSize-1)
       
        if (temp == NULL) { //make sure memory was allocated properly
            cout << "Error: memory could not be allocated";
            return false;
        }
        
        listSize--;
        listUsed--;

        for (int i = 0, j = 0; i <= listSize, j < listSize; i++) {
            if (i != index) { //check that the index is not the location of the object to be removed
                temp[j] = resourceList[i]; //set temp to the object in resourceList
                j++; //increment j only if the index is not the one which we are removing

            }

        }
        
        delete[] resourceList; //delete the old array
        resourceList = temp; //change where resourceList points to
        
        return true;
    }

    bool Task::removeResource(string res) {
        int i = 0;
        bool isFound;
        
        for (; i < listSize && !isFound; isFound = !res.compare(resourceList[i]), i++); //search for the string in the array
        
        if (isFound) {

            return removeResource(i); //if it is found remove it 
        }
        else {
            cout << "Error. Chore not found.\n";
            return false;
        }
    }
	Task* Task::getLink(){
		if (next == NULL){
			return NULL;
		}
		return next;

	}
	bool Task::setLink(const Task& t){
		next = new Task(t);
		return true;
	}
	bool Task::setLink(Task* t){
		next = t;
		return true;
	}
}

