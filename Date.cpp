/*********************************************************
 * file name: Date.cpp
 * programmer name: Dylan Kozicki & Zachary McMinn
 * date created: February 17, 2014
 * date of last revision: February 17, 2014
 * details of the revision: none
 * int description:  <enter description here>
 **********************************************************/

#include "Date.h"
#include <cassert>
#include <iostream>
#include <sstream>
using std::ostringstream;


namespace Cmpsc122Project {

    Date::Date(int month, int day, int year) {
        setYear(year);

        setMonth(month);

        setDay(day);
    }

    Date::Date() {

    }

    Date::Date(const Date& orig) {
        setYear(orig.getYear());

        setMonth(orig.getMonth());
        setDay(orig.getDay());

    }

    int Date::getMonth() const {
        return date[0];
    }

    int Date::getDay() const {
        return date[1];
    }

    int Date::getYear() const {
        return date[2];
    }

    void Date::setMonth(int m) {
        assert(m > 0);

        assert(m <= 12);
        date[0] = m;
        month.name = MONTHS[m - 1].name;
        month.days = MONTHS[m - 1].days;
        if (m == 2) {
            if (date[2] % 4 == 0 && (date[2] % 100 != 0 || date[2] % 400 == 0)) {
                month.days = 29;
            }
        }


    }

    void Date::setDay(int day) {
        assert(day > 0);
        assert(day <= month.days);
        date[1] = day;
    }

    void Date::setYear(int year) {

        assert(year > 2000);

        date[2] = year;
        if (date[0] == 2) {
            setMonth(2); //fixes leapyear
        }
    }

    bool operator ==(const Date& lhs, const Date& rhs) {
        if (lhs.getDay() != rhs.getDay()) {
            return false;
        }
        if (lhs.getMonth() != rhs.getMonth()) {
            return false;
        }
        if (lhs.getYear() != rhs.getYear()) {
            return false;
        }
        return true;
    }

    bool operator !=(const Date& lhs, const Date& rhs) {
        return !(lhs == rhs);
    }

    string Date::toString() {
        ostringstream d, y;
        d << date[1];
        y << date[2];
        return month.name + " " + d.str() + ", " + y.str();
    }

    Date::~Date() {
    }

}
