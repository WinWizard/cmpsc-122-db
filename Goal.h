/*********************************************************
 * file name: Goal.h
 * programmer name: Dylan Kozicki
 * date created: April 6, 2014
 * date of last revision: April 6, 2014
 * details of the revision: none
 * short description:  
 **********************************************************/

#ifndef GOAL_H
#define	GOAL_H
#include "Date.h"
#include "Objective.h"
#include "Task.h"
namespace Cmpsc122Project{
class Goal {
public:
    Goal();
    Goal(const Goal& orig);
    ~Goal();
    Goal* next;
     //operators
        Goal& operator =(const Goal& orig);
        friend bool operator ==(const Goal& lhs, const Goal& rhs);
        friend bool operator !=(const Goal& lhs, const Goal& rhs);
        void swap(const Goal& orig); //swaps two objectives

        //accessor functions
        string getName() const; //gets the name of the objective
        string getDesc() const; //gets the description of the objective
        int getPrio() const; //gets the priority of the objective
        string getCatAsString() const; //gets the category as a string
        int getCatAsInt() const; //gets the category as a numerical value
        Date getDueDate() const; //returns the due date of the objective
        Date getStartDate() const; //returns the start date of the objective
        int getTime() const; //gets the time required to finish (again type may change)
        Objective* getObjectives() const; //gets the list of goals;
        bool getStatus() const; //gets the completion status of the objective
        int getObjectivesSize() const; //gets the size of the task list
        

        //mutator functions
        void setName(string Name);
        void setDesc(string desc);
        void setPrio(int prio);
        void setCat(int cat); //sets the category via int value (1-4)
        void setCat(string cat); //sets the category via its string name (Quad 1, Quad 2, Quad 3, Quad 4)
        void setStatus(bool stat); //sets status of completion to true if done, false if not
        void setTime(int t); // sets the time as an integer
        void setStartDate(int m, int d, int y); //sets start date from Date.h
        void setDueDate(int m, int d, int y); //Unsure what is needed for it at the moment -Ryan

       

        //mutators for objectiveList
        bool removeObjective(int index); //removes objective at index
        bool objectiveListClear();
        bool addObjective(const Objective& objective);

        //File IO
        void readData(ifstream& filePath); //reads the data from a file
        void writeData(string filePath); //writes the data to a file
        void clearOutStream(string filename); //used to clear destination output file
        bool setLink(const Goal& t);
        bool setLink(Goal* t);
        Goal* getLink();
        
        
private:
    Objective* objectiveList;
    int objectiveListSize;
     string name; //objective name
        string description; //objective description
        int category; //Objective category (int from 1-4)
        int priority; //objective priority (int from 1-5)
        Date dueDate; //Date the objective is due from Date.h
        Date startDate; //Date the objective is assigned from Date.h
        int time; //time required to finish (type subject to change)
        bool isComplete; //status of completion of the objective

};
}
#endif	/* GOAL_H */

